package com.travix.medusa.busyflights.crazyair;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.mapper.crazyair.CrazyAirRequestMapper;
import com.travix.medusa.busyflights.mapper.toughjet.ToughJetRequestMapper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link ToughJetRequestMapper}
 *
 * @author acozma
 */
public class CrazyAirRequestMapperShould {

	private CrazyAirRequestMapper mapper;

	@Before
	public void setUp(){
		mapper = new CrazyAirRequestMapper();
	}

	@Test
	public void mapABusyFlightsRequestToACrazyAirRequest() {

		//setup
		BusyFlightsRequest busyFlightsRequest = new BusyFlightsRequest();
		busyFlightsRequest.setDepartureDate("2019-11-30T09:00:00");
		busyFlightsRequest.setReturnDate("2019-12-02T23:00:00");
		busyFlightsRequest.setDestination("London");
		busyFlightsRequest.setOrigin("Iasi");
		busyFlightsRequest.setNumberOfPassengers(1);

		//execute
		CrazyAirRequest crazyAirRequest = mapper.map(busyFlightsRequest);

		//verify
		CrazyAirRequest expectedCrazyAirRequest = new CrazyAirRequest();
		expectedCrazyAirRequest.setPassengerCount(1);
		expectedCrazyAirRequest.setDepartureDate("2019-11-30T09:00:00");
		expectedCrazyAirRequest.setReturnDate("2019-12-02T23:00:00");
		expectedCrazyAirRequest.setDestination("London");
		expectedCrazyAirRequest.setOrigin("Iasi");

		assertEquals(expectedCrazyAirRequest,crazyAirRequest);
	}
}