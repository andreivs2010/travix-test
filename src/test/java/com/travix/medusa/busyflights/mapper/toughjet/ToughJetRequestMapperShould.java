package com.travix.medusa.busyflights.mapper.toughjet;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test class for {@link ToughJetRequestMapper}
 *
 * @author acozma
 */
public class ToughJetRequestMapperShould {

	private ToughJetRequestMapper mapper;

	@Before
	public void setUp(){
		mapper = new ToughJetRequestMapper();
	}

	@Test
	public void mapABusyFlightsRequestToAToughJetRequest() {

		//setup
		BusyFlightsRequest busyFlightsRequest = new BusyFlightsRequest();
		busyFlightsRequest.setDepartureDate("2019-11-30T09:00:00");
		busyFlightsRequest.setReturnDate("2019-12-02T23:00:00");
		busyFlightsRequest.setDestination("London");
		busyFlightsRequest.setOrigin("Iasi");
		busyFlightsRequest.setNumberOfPassengers(1);

		//execute
		ToughJetRequest toughJetRequest = mapper.map(busyFlightsRequest);

		//verify
		ToughJetRequest expectedToughJetRequest = new ToughJetRequest();
		expectedToughJetRequest.setNumberOfAdults(1);
		expectedToughJetRequest.setInboundDate("2019-11-30T09:00:00");
		expectedToughJetRequest.setOutboundDate("2019-12-02T23:00:00");
		expectedToughJetRequest.setTo("London");
		expectedToughJetRequest.setFrom("Iasi");

		assertEquals(expectedToughJetRequest,toughJetRequest);
	}
}