package com.travix.medusa.busyflights.mapper.toughjet;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import org.springframework.stereotype.Component;

/**
 * Mapper used to map a {@link BusyFlightsRequest} to a {@link ToughJetRequest}
 *
 * @author acozma
 */
@Component
public class ToughJetRequestMapper {

	/**
	 * Maps a {@link BusyFlightsRequest} to a {@link ToughJetRequest}
	 *
	 * @param busyFlightsRequest - the {@link BusyFlightsRequest}
	 *
	 * @return the instance of {@link ToughJetRequest}
	 */
	public ToughJetRequest map(BusyFlightsRequest busyFlightsRequest) {

		ToughJetRequest request = new ToughJetRequest();

		request.setFrom(busyFlightsRequest.getOrigin());
		request.setTo(busyFlightsRequest.getDestination());
		request.setOutboundDate(busyFlightsRequest.getReturnDate());
		request.setInboundDate(busyFlightsRequest.getDepartureDate());
		request.setNumberOfAdults(busyFlightsRequest.getNumberOfPassengers());

		return request;
	}
}
