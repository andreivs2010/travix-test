package com.travix.medusa.busyflights.mapper.crazyair;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;

import org.springframework.stereotype.Component;

/**
 * Mapper used to map a {@link BusyFlightsRequest} to a {@link CrazyAirRequest}
 *
 * @author acozma
 */
@Component
public class CrazyAirRequestMapper {

	/**
	 * Maps a {@link BusyFlightsRequest} to a {@link CrazyAirRequest}
	 *
	 * @param busyFlightsRequest - the {@link BusyFlightsRequest}
	 *
	 * @return the instance of {@link CrazyAirRequest}
	 */
	public CrazyAirRequest map(BusyFlightsRequest busyFlightsRequest) {

		CrazyAirRequest request = new CrazyAirRequest();

		request.setOrigin(busyFlightsRequest.getOrigin());
		request.setDestination(busyFlightsRequest.getDestination());
		request.setReturnDate(busyFlightsRequest.getReturnDate());
		request.setDepartureDate(busyFlightsRequest.getDepartureDate());
		request.setPassengerCount(busyFlightsRequest.getNumberOfPassengers());

		return request;
	}
}
