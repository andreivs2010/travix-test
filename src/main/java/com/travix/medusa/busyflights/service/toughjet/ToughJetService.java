package com.travix.medusa.busyflights.service.toughjet;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;

import java.io.IOException;
import java.util.List;

public interface ToughJetService {

	List<ToughJetResponse> getToughJetFares(BusyFlightsRequest busyFlightsRequest) throws IOException;
}
