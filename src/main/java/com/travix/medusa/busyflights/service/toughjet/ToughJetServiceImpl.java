package com.travix.medusa.busyflights.service.toughjet;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import com.travix.medusa.busyflights.httpclient.toughjet.ToughJetHttpClient;
import com.travix.medusa.busyflights.mapper.toughjet.ToughJetRequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

/**
 * @author acozma
 */
@Service("toughJetService")
public class ToughJetServiceImpl implements ToughJetService {

	@Autowired
	private ToughJetRequestMapper toughJetRequestMapper;

	@Autowired
	private ToughJetHttpClient toughJetHttpClient;


	@Override
	public List<ToughJetResponse> getToughJetFares(BusyFlightsRequest busyFlightsRequest) throws IOException {

		ToughJetRequest request = toughJetRequestMapper.map(busyFlightsRequest);

		return toughJetHttpClient.invoke(request);

	}
}
