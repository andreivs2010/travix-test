package com.travix.medusa.busyflights.service.busyflights;

import com.travix.medusa.busyflights.domain.busyflights.Airline;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import com.travix.medusa.busyflights.service.crazyair.CrazyAirService;
import com.travix.medusa.busyflights.service.toughjet.ToughJetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author acozma
 */
@Service("busyFlightsService")
public class BusyFlightsService {

	@Autowired
	private CrazyAirService crazyAirService;

	@Autowired
	private ToughJetService toughJetService;

	public List<BusyFlightsResponse> getFlights(BusyFlightsRequest request) throws IOException {

		List<BusyFlightsResponse> busyFlightsResponses = new ArrayList<>();

		List<CrazyAirResponse> crazyAirFares = crazyAirService.getCrazyAirFares(request);

		List<ToughJetResponse> toughJetFares = toughJetService.getToughJetFares(request);

		if (crazyAirFares.isEmpty() && toughJetFares.isEmpty()) {
			return busyFlightsResponses;

		}

		return aggregateResults(crazyAirFares,toughJetFares);

	}

	private List<BusyFlightsResponse> aggregateResults(List<CrazyAirResponse> crazyAirFares, List<ToughJetResponse> toughJetFares) {

		List<BusyFlightsResponse> flightsFares = new ArrayList<>();

		for(CrazyAirResponse crazyAirResponse : crazyAirFares){
			BusyFlightsResponse flightFare = new BusyFlightsResponse();
			flightFare.setAirline(crazyAirResponse.getAirline());
			flightFare.setSupplier(Airline.CRAZY_AIR);
			BigDecimal fare = new BigDecimal(crazyAirResponse.getPrice());
			fare = fare.setScale(2, RoundingMode.HALF_EVEN);
			flightFare.setFare(fare);
			flightFare.setDepartureAirportCode(crazyAirResponse.getDepartureAirportCode());
			flightFare.setDestinationAirportCode(crazyAirResponse.getDestinationAirportCode());
			flightFare.setDepartureDate(LocalDateTime.parse(crazyAirResponse.getDepartureDate()));
			flightFare.setArrivalDate(LocalDateTime.parse(crazyAirResponse.getArrivalDate()));
			flightsFares.add(flightFare);
		}

		for(ToughJetResponse toughJetResponse : toughJetFares){
			BusyFlightsResponse flightFare = new BusyFlightsResponse();
			flightFare.setAirline(toughJetResponse.getCarrier());
			flightFare.setSupplier(Airline.TOUGH_JET);
			BigDecimal fare = new BigDecimal(toughJetResponse.getBasePrice());
			fare = fare.setScale(2, RoundingMode.HALF_EVEN);
			flightFare.setFare(fare);

			flightFare.setDepartureAirportCode(toughJetResponse.getDepartureAirportName());
			flightFare.setDestinationAirportCode(toughJetResponse.getArrivalAirportName());
			flightFare.setDepartureDate(LocalDateTime.ofInstant(toughJetResponse.getInboundDateTime(), ZoneOffset.UTC));
			flightFare.setArrivalDate(LocalDateTime.ofInstant(toughJetResponse.getOutboundDateTime(), ZoneOffset.UTC));
			flightsFares.add(flightFare);
		}


		flightsFares.sort((o1, o2) -> o2.getFare().compareTo(o1.getFare()));

		Collections.reverse(flightsFares);

		return flightsFares;


	}

}
