package com.travix.medusa.busyflights.service.crazyair;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;

import java.io.IOException;
import java.util.List;

public interface CrazyAirService {

	List<CrazyAirResponse> getCrazyAirFares(BusyFlightsRequest busyFlightsRequest) throws IOException;
}
