package com.travix.medusa.busyflights.service.exception;


/**
 * @author acozma
 */
public class BusyFlightsException extends Exception{

	public enum Reason{
		NUMBER_OF_PASSENGERS_EXCEEDED;
	}

	private Reason reason;

	public BusyFlightsException(String message, Reason reason) {
		super(message);
		this.reason = reason;
	}

	public Reason getReason() {
		return reason;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}
}
