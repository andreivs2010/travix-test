package com.travix.medusa.busyflights.service.crazyair;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.httpclient.crazyair.CrazyAirHttpClient;
import com.travix.medusa.busyflights.mapper.crazyair.CrazyAirRequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * @author acozma
 */
@Service("crazyAirService")
public class CrazyAirServiceImpl implements CrazyAirService {

	@Autowired
	private CrazyAirRequestMapper crazyAirRequestMapper;

	@Autowired
	private CrazyAirHttpClient crazyAirHttpClient;

	@Override
	public List<CrazyAirResponse> getCrazyAirFares(BusyFlightsRequest busyFlightsRequest) throws IOException {

		CrazyAirRequest request = crazyAirRequestMapper.map(busyFlightsRequest);

		return crazyAirHttpClient.invoke(request);
	}
}
