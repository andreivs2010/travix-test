package com.travix.medusa.busyflights.service;

import com.travix.medusa.busyflights.service.exception.BusyFlightsException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author acozma
 */
@Component
public class PassengerValidationService {

	public void validateNumberOfPassengers(int numberOfPass) throws  BusyFlightsException{

		if(numberOfPass>4){

			throw new BusyFlightsException("Exceeded number of passengers",BusyFlightsException.Reason.NUMBER_OF_PASSENGERS_EXCEEDED);
		}

	}
}
