package com.travix.medusa.busyflights.domain.toughjet;

import java.time.Instant;

public class ToughJetResponse {

	private String carrier;
	private double basePrice;
	private double tax;
	private double discount;
	private String departureAirportName;
	private String arrivalAirportName;
	private Instant outboundDateTime;
	private Instant inboundDateTime;

	public ToughJetResponse(String carrier, double basePrice, double tax, double discount,
							String departureAirportName, String arrivalAirportName, Instant outboundDateTime,
							Instant inboundDateTime) {
		this.carrier = carrier;
		this.basePrice = basePrice;
		this.tax = tax;
		this.discount = discount;
		this.departureAirportName = departureAirportName;
		this.arrivalAirportName = arrivalAirportName;
		this.outboundDateTime = outboundDateTime;
		this.inboundDateTime = inboundDateTime;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(final String carrier) {
		this.carrier = carrier;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(final double basePrice) {
		this.basePrice = basePrice;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(final double tax) {
		this.tax = tax;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(final double discount) {
		this.discount = discount;
	}

	public String getDepartureAirportName() {
		return departureAirportName;
	}

	public void setDepartureAirportName(final String departureAirportName) {
		this.departureAirportName = departureAirportName;
	}

	public String getArrivalAirportName() {
		return arrivalAirportName;
	}

	public void setArrivalAirportName(final String arrivalAirportName) {
		this.arrivalAirportName = arrivalAirportName;
	}

	public Instant getOutboundDateTime() {
		return outboundDateTime;
	}

	public void setOutboundDateTime(final Instant outboundDateTime) {
		this.outboundDateTime = outboundDateTime;
	}

	public Instant getInboundDateTime() {
		return inboundDateTime;
	}

	public void setInboundDateTime(final Instant inboundDateTime) {
		this.inboundDateTime = inboundDateTime;
	}
}
