package com.travix.medusa.busyflights.domain.busyflights;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

public class BusyFlightsResponse {

	private String airline;

	//At this stage having in mind the limited number of airline it makes more sense to use
	//an enum because the data dictionary is small and it offers validation and iteration out-of-the-box
	//TODO replace with string if list grows bigger in which case an enum can't maintain scalability (too many values)
	private Airline supplier;

	private BigDecimal fare;

	private String departureAirportCode;

	private String destinationAirportCode;

	private LocalDateTime departureDate;

	private LocalDateTime arrivalDate;

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public Airline getSupplier() {
		return supplier;
	}

	public void setSupplier(Airline supplier) {
		this.supplier = supplier;
	}

	public BigDecimal getFare() {
		return fare;
	}

	public void setFare(BigDecimal fare) {
		this.fare = fare;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	public LocalDateTime getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(LocalDateTime departureDate) {
		this.departureDate = departureDate;
	}

	public LocalDateTime getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(LocalDateTime arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		BusyFlightsResponse that = (BusyFlightsResponse) o;
		return Objects.equals(airline, that.airline) &&
				supplier == that.supplier &&
				Objects.equals(fare, that.fare) &&
				Objects.equals(departureAirportCode, that.departureAirportCode) &&
				Objects.equals(destinationAirportCode, that.destinationAirportCode) &&
				Objects.equals(departureDate, that.departureDate) &&
				Objects.equals(arrivalDate, that.arrivalDate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(airline, supplier, fare, departureAirportCode, destinationAirportCode, departureDate,
				arrivalDate);
	}

	@Override
	public String toString() {
		return "BusyFlightsResponse{" +
				"airline='" + airline + '\'' +
				", supplier=" + supplier +
				", fare=" + fare +
				", departureAirportCode='" + departureAirportCode + '\'' +
				", destinationAirportCode='" + destinationAirportCode + '\'' +
				", departureDate=" + departureDate +
				", arrivalDate=" + arrivalDate +
				'}';
	}
}
