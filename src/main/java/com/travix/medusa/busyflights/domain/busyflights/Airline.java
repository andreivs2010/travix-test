package com.travix.medusa.busyflights.domain.busyflights;

/**
 * Enum used to indicate the used airline for aggregating results
 *
 * @author acozma
 */
public enum Airline {

	CRAZY_AIR,

	TOUGH_JET
}
