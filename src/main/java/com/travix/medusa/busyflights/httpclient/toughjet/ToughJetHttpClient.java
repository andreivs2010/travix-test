package com.travix.medusa.busyflights.httpclient.toughjet;

import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Mocked http client for 'posting' requests to the ToughJet Api
 *
 * @author acozma
 */
@Component
public class ToughJetHttpClient {

	private static List<ToughJetResponse> dummyData;

	static {
		dummyData = populateDummyData();
	}


	private static List<ToughJetResponse> populateDummyData() {
		List<ToughJetResponse> data = new ArrayList<>();

		//Just as a side note, the base price provided here takes into account the number of passengers
		//in reality, the api would calculate the price according to the request parameter
		data.add(new ToughJetResponse("Wizzair", 100.40, 5.50, 10.50, "IAS", "LTN", Instant.parse("2019-11-30T12:00:00.00Z"), Instant.parse("2019-12-03T12:00:00.00Z")));
		data.add(new ToughJetResponse("BlueAir", 110.40, 5.50, 10.50, "IAS", "LTN", Instant.parse("2019-11-30T12:00:00.00Z"), Instant.parse("2019-12-03T12:00:00.00Z")));
		data.add(new ToughJetResponse("AirFrance", 120.40, 5.50, 10.50, "LTN", "IAS", Instant.parse("2019-11-30T12:00:00.00Z"), Instant.parse("2019-12-03T12:00:00.00Z")));
		data.add(new ToughJetResponse("Wizzair", 130.40, 5.50, 10.50, "LTN", "IAS", Instant.parse("2019-11-30T12:00:00.00Z"), Instant.parse("2019-12-03T12:00:00.00Z")));
		data.add(new ToughJetResponse("BlueAir", 140.40, 5.50, 10.50, "IAS", "LHT", Instant.parse("2019-11-30T12:00:00.00Z"), Instant.parse("2019-12-03T12:00:00.00Z")));
		data.add(new ToughJetResponse("Tarom", 150.40, 5.50, 10.50, "IAS", "LHT", Instant.parse("2019-11-30T12:00:00.00Z")	, Instant.parse("2019-12-03T12:00:00.00Z")));

		data.add(new ToughJetResponse("Wizzair", 120.40, 5.50, 10.50, "IAS", "LTN", Instant.parse("2019-11-30T12:00:00.00Z"), Instant.parse("2019-12-03T12:00:00.00Z")));
		data.add(new ToughJetResponse("AirFrance", 130.40, 5.50, 10.50, "LTN", "IAS", Instant.parse("2019-11-30T12:00:00.00Z"), Instant.parse("2019-12-03T12:00:00.00Z")));
		data.add(new ToughJetResponse("BlueAir", 190.40, 5.50, 10.50, "IAS", "LTN", Instant.parse("2019-11-30T12:00:00.00Z"), Instant.parse("2019-12-03T12:00:00.00Z")));
		data.add(new ToughJetResponse("Tarom", 90.40, 5.50, 10.50, "LHT", "IAS", Instant.parse("2019-11-30T12:00:00.00Z"),	Instant.parse("2019-12-03T12:00:00.00Z")));

		return data;
	}

	public List<ToughJetResponse> invoke(ToughJetRequest request) {

		//This is just a mock-up functionality in order to produce some results in requested time frame
		//In reality departures would be correlated with the arrivals
		//TODO fix correlation between departures and arrivals
		List<ToughJetResponse> departures = dummyData.stream()
				.filter(response -> response.getDepartureAirportName().equals(request.getFrom()))
				.filter(response -> response.getArrivalAirportName().equals(request.getTo()))
				.filter(response -> isInSameDayDeparture(request, response))

				.collect(Collectors.toList());

		List<ToughJetResponse> arrivals = dummyData.stream()
				.filter(response -> response.getDepartureAirportName().equals(request.getTo()))
				.filter(response -> response.getArrivalAirportName().equals(request.getFrom()))
				.filter(response -> isInSameDayReturn(request, response))

				.collect(Collectors.toList());

		departures.addAll(arrivals);
		return departures;

	}

	private boolean isInSameDayReturn(ToughJetRequest request, ToughJetResponse response) {
		LocalDate requestDate = LocalDate.parse(request.getInboundDate());
		LocalDateTime responseDate = LocalDateTime.ofInstant(response.getOutboundDateTime(), ZoneOffset.UTC);
		return requestDate.equals(responseDate.toLocalDate());
	}

	private boolean isInSameDayDeparture(ToughJetRequest request, ToughJetResponse response) {
		LocalDate requestDate = LocalDate.parse(request.getOutboundDate());
		LocalDateTime responseDate = LocalDateTime.ofInstant(response.getOutboundDateTime(), ZoneOffset.UTC);
		return requestDate.equals(responseDate.toLocalDate());
	}


}
