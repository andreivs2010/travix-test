package com.travix.medusa.busyflights.httpclient.crazyair;

import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Mocked http client for 'posting' requests to the CrazyAir Api
 *
 * @author acozma
 */
@Component
public class CrazyAirHttpClient {

	private static List<CrazyAirResponse> dummyData;

	static {
		dummyData = populateDummyData();
	}


	private static List<CrazyAirResponse> populateDummyData() {
		List<CrazyAirResponse> data = new ArrayList<>();

		//Just as a side note, the base price provided here takes into account the number of passengers
		//in reality, the api would calculate the price according to the request parameter
		data.add(new CrazyAirResponse("Wizzair", 100.40, "E",  "IAS", "LTN", "2019-11-30T12:00:00", "2019-12" +
				"-03T12:00:00"));
		data.add(new CrazyAirResponse("BlueAir", 110.40, "E",  "IAS", "LTN", "2019-11-30T12:00:00", "2019-12" +
				"-03T12:00:00"));
		data.add(new CrazyAirResponse("AirFrance", 120.40, "E",  "LTN", "IAS", "2019-11-30T12:00:00", "2019-12" +
				"-03T12:00:00"));
		data.add(new CrazyAirResponse("Wizzair", 130.40, "B",  "LTN", "IAS", "2019-11-30T12:00:00", "2019-12" +
				"-03T12:00:00"));
		data.add(new CrazyAirResponse("BlueAir", 140.40, "B",  "IAS", "LHT", "2019-11-30T12:00:00", "2019-12" +
				"-03T12:00:00"));
		data.add(new CrazyAirResponse("Tarom", 150.40, "E",  "IAS", "LHT", "2019-11-30T12:00:00", "2019-12" +
				"-03T12:00:00"));

		data.add(new CrazyAirResponse("Wizzair", 120.40, "E",  "IAS", "LTN", "2019-11-30T12:00:00", "2019-12" +
				"-03T12:00:00"));
		data.add(new CrazyAirResponse("AirFrance", 130.40, "B", "LTN", "IAS", "2019-11-30T12:00:00", "2019-12" +
				"-03T12:00:00"));
		data.add(new CrazyAirResponse("BlueAir", 190.40, "B", "IAS", "LTN", "2019-11-30T12:00:00", "2019-12" +
				"-03T12:00:00"));
		data.add(new CrazyAirResponse("Tarom", 90.40, "B",  "LHT", "IAS", "2019-11-30T12:00:00", "2019-12-03T12" +
				":00:00"));

		return data;
	}


	public List<CrazyAirResponse> invoke(CrazyAirRequest request) throws IOException {

		//This is just a mock-up functionality in order to produce some results in requested time frame
		//In reality departures would be correlated with the arrivals
		//TODO fix correlation between departures and arrivals
		List<CrazyAirResponse> departures = dummyData.stream()
				.filter(response -> response.getDestinationAirportCode().equals(request.getDestination()))
				.filter(response -> response.getDepartureAirportCode().equals(request.getOrigin()))
				.filter(response -> isInSameDayDeparture(request, response))

				.collect(Collectors.toList());

		List<CrazyAirResponse> arrivals = dummyData.stream()
				.filter(response -> response.getDepartureAirportCode().equals(request.getDestination()))
				.filter(response -> response.getDestinationAirportCode().equals(request.getOrigin()))
				.filter(response -> isInSameDayReturn(request, response))

				.collect(Collectors.toList());

		departures.addAll(arrivals);
		return departures;

	}

	private boolean isInSameDayReturn(CrazyAirRequest request, CrazyAirResponse response) {
		LocalDate requestDate = LocalDate.parse(request.getReturnDate());
		LocalDateTime responseDate = LocalDateTime.parse(response.getDepartureDate());
		return requestDate.equals(responseDate.toLocalDate());
	}

	private boolean isInSameDayDeparture(CrazyAirRequest request, CrazyAirResponse response) {
		LocalDate requestDate = LocalDate.parse(request.getDepartureDate());
		LocalDateTime responseDate = LocalDateTime.parse(response.getDepartureDate());
		return requestDate.equals(responseDate.toLocalDate());
	}


}
