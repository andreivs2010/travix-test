package com.travix.medusa.busyflights.controller;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;
import com.travix.medusa.busyflights.service.PassengerValidationService;
import com.travix.medusa.busyflights.service.busyflights.BusyFlightsService;
import com.travix.medusa.busyflights.service.exception.BusyFlightsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * @author acozma
 */
@RestController
@RequestMapping("/flights")
public class BusyFlightsController {

	@Autowired
	private BusyFlightsService busyFlightsService;

	@Autowired
	private PassengerValidationService validationService;

	@RequestMapping(value = "/search/{origin}/{destination}", method = RequestMethod.GET)
	public ResponseEntity<List<BusyFlightsResponse>> getFlights(@PathVariable("origin") String origin, @PathVariable(
			"destination") String destination,
																@RequestParam("departureDate") String departureDate,
																@RequestParam("returnDate") String returnDate
			, @RequestParam("numberOfPass") int numberOfPass) throws IOException {
		try {

			validationService.validateNumberOfPassengers(numberOfPass);
		} catch (BusyFlightsException exception) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}

		BusyFlightsRequest request = new BusyFlightsRequest();
		request.setOrigin(origin);
		request.setDestination(destination);
		request.setReturnDate(returnDate);
		request.setDepartureDate(departureDate);
		request.setNumberOfPassengers(numberOfPass);
		List<BusyFlightsResponse> busyFlightsResponses = busyFlightsService.getFlights(request);

		return new ResponseEntity<>(busyFlightsResponses, HttpStatus.OK);
	}


}
